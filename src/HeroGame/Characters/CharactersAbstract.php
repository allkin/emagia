<?php
/**
 * Created by PhpStorm.
 * User: ainz
 * Date: 10/7/18
 * Time: 11:39 AM
 */

namespace HeroGame\Characters;

use HeroGame\Game\Helper;

/**
 * Class CharactersAbstract
 * @package HeroGame\Characters
 */
abstract class CharactersAbstract implements CharactersInterface
{

    /**
     * @var $name string
     */
    protected $name;

    /**
     * @var $health float
     */
    protected $health;

    /**
     * @var $strength int
     */
    protected $strength;

    /**
     * @var $defence int
     */
    protected $defence;

    /**
     * @var $speed int
     */
    protected $speed;

    /**
     * @var $luck int
     */
    protected $luck;

    /**
     * We need a var to store the health left because we will use the original health for recover skills
     * @var float
     */
    protected $healthLeft;


    /**
     * Here we deviate a little from the test requirements and implement a common trait
     * Both heroes and wild beasts can have skills
     * @var $skills array
     */
    protected $skills = [];


    public function __construct()
    {

    }

    #######################
    ## GETTERS & SETTERS ##
    #######################


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return CharactersAbstract
     */
    public function setName(string $name): CharactersAbstract
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return float
     */
    public function getHealth(): float
    {
        return $this->health;
    }

    /**
     * @param float $healt
     * @return CharactersAbstract
     */
    public function setHealth(float $health): CharactersAbstract
    {
        $this->health = $health;
        return $this;
    }

    /**
     * @return int
     */
    public function getStrength(): int
    {
        return $this->strength;
    }

    /**
     * @param int $strength
     * @return CharactersAbstract
     */
    public function setStrength(int $strength): CharactersAbstract
    {
        $this->strength = $strength;

        return $this;
    }

    /**
     * @return int
     */
    public function getDefence(): int
    {
        return $this->defence;
    }


    /**
     * @param int $defence
     * @return int
     */
    public function setDefence(int $defence): CharactersAbstract
    {
        $this->defence = $defence;
        return $this;
    }

    /**
     * @return int
     */
    public function getSpeed(): int
    {
        return $this->speed;
    }

    /**
     * @param int $speed
     * @return CharactersAbstract
     */
    public function setSpeed(int $speed): CharactersAbstract
    {
        $this->speed = $speed;
        return $this;
    }

    /**
     * @return int
     */
    public function getLuck(): int
    {
        return $this->luck;
    }

    /**
     * @param int $luck
     * @return CharactersAbstract
     */
    public function setLuck(int $luck): CharactersAbstract
    {
        $this->luck = $luck;
        return $this;
    }

    /**
     * @return float
     */
    public function getHealthLeft(): float
    {
        return $this->healthLeft;
    }

    /**
     * @param float $health
     * @return CharactersAbstract
     */
    public function setHealthLeft(float $health): CharactersAbstract
    {
        $this->healthLeft = $health;
        return $this;
    }


    /**
     * @return array
     */
    public function getSkills(): array
    {
        return $this->skills;
    }


    /**
     * @return bool
     * @throws \Exception
     */
    public function isLuckyThisRound(): bool
    {
        return Helper::getRandom(1, 100) <= $this->getLuck();
    }

    /**
     * @param string $skillName
     * @param int $chance
     * @return CharactersAbstract
     */
    public function addSkill(string $skillName, int $chance = 5): CharactersAbstract
    {
        $className = sprintf("HeroGame\Skills\%s", $skillName);
        if (class_exists($className)) {
            $skill = new $className();
            $skillType = $skill->getType();

            if (empty($this->skills[$skillType])) $this->skills[$skillType] = array();
            $this->skills[$skillType][$skillName] = $skill;

            $this->skills[$skillType][$skillName]->setChance($chance);
        }
        return $this;
    }

    /**
     * @return bool
     */
    public function isAlive(): bool
    {
        return $this->healthLeft > 0;
    }


}