<?php
/**
 * Created by PhpStorm.
 * User: ainz
 * Date: 10/7/18
 * Time: 11:35 AM
 */

namespace HeroGame\Characters;

/**
 * Interface CharactersInterface
 * @package HeroGame\Characters
 */
interface CharactersInterface
{

    /**
     * Mandatory method to check if character is alive
     *
     * @return bool
     */
    public function isAlive(): bool;

}