<?php
/**
 * Created by PhpStorm.
 * User: ainz
 * Date: 10/7/18
 * Time: 12:20 PM
 */

namespace HeroGame\Skills;

use HeroGame\Characters\CharactersAbstract;

/**
 * Interface SkillsInterface
 * @package HeroGame\Skills
 */
interface SkillsInterface
{

    /**
     * We need the charact object for skills that alters stats directly
     * @param CharactersAbstract $charact
     * @param int $currentRound
     * @param float $damage
     * @return mixed
     */
    public function useSkill(CharactersAbstract &$charact, int $currentRound, float $damage): float;
}