<?php
/**
 * Created by PhpStorm.
 * User: ainz
 * Date: 10/7/18
 * Time: 1:57 PM
 */

namespace HeroGame\Skills;

use HeroGame\Characters\CharactersAbstract;

/**
 * Magic Shield enables user to take half damage
 *
 * Class MagicShield
 * @package HeroGame\Skills
 */
class MagicShield extends SkillsAbastract implements SkillsInterface
{

    /**
     * @var string
     */
    protected $skillName = 'Magic Shield';

    /**
     * @var int
     */
    protected $chance = 8;

    /**
     * @var int
     */
    protected $duration = 1;


    /**
     * @var bool
     */
    protected $status = false;

    /**
     * @var int
     */
    protected $roundActivated = 0;

    /**
     * @var int
     */
    protected $type = 2;

    /**
     * @param CharactersAbstract $charact
     * @param int $currentRound
     * @param float $damage
     * @return float|int
     */
    public function useSkill(CharactersAbstract &$charact, int $currentRound, float $damage): float
    {

        return $damage / 2;

    }
}