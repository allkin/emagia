<?php
/**
 * Created by PhpStorm.
 * User: ainz
 * Date: 10/7/18
 * Time: 12:22 PM
 */

namespace HeroGame\Skills;

use HeroGame\Characters\CharactersAbstract;
use HeroGame\Game\Helper;

/**
 * Skills types:
 * 1 - Attack
 * 2 - Defence
 * 3 - Stats
 *
 * Class SkillsAbastract
 * @package HeroGame\Skills
 */
abstract class SkillsAbastract
{

    /**
     * Mapping skills types
     */
    const ATTACK = 1;

    const DEFENCE = 2;

    const RECOVER = 3;


    /**
     * @var $skillName string
     */
    protected $skillName;

    /**
     * Imagine how powerful a skill duration can be. We are in 2018; we can't create a simple game ^_^
     * The duration is expressed in rounds.
     * @var $duration int
     */
    protected $duration;


    /**
     * Every skill has a default chance to activate. Every character can have a custom chance.
     * @var $chance int
     */
    protected $chance;

    /**
     * @var $status bool
     */
    protected $status;

    /**
     * When was activated
     * @var $roundActivated int;
     */
    protected $roundActivated;

    /**
     * @var $type int
     */
    protected $type;


    public function __construct()
    {

    }

    #############
    ## GETTERS ##
    #############

    /**
     * @return string
     */
    public function getSkillName(): string
    {
        return $this->skillName;
    }

    /**
     * I don't want to add a setter for the duration. I like to think skills are somewhat static
     * and can't be altered once the game has started
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration;
    }

    /**
     * @return bool
     */
    public function getStatus(): bool
    {
        return $this->status;
    }

    /**
     * @return int
     */
    public function getRoundActivated(): int
    {
        return $this->roundActivated;
    }

    /**
     * @return int
     */
    public function getChance(): int
    {
        return $this->chance;
    }

    public function getType(): int
    {
        return $this->type;
    }


    #############
    ## SETTERS ##
    #############

    /**
     * @param int $chance
     * @return SkillsAbastract
     */
    public function setChance(int $chance): SkillsAbastract
    {
        $this->chance = $chance;
        return $this;
    }


    /**
     * @param bool $status
     * @return SkillsAbastract
     */
    public function setStatus(bool $status): SkillsAbastract
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @param int $round
     * @return CharactersAbstract
     */
    public function setRoundActivated(int $round): SkillsAbastract
    {
        $this->roundActivated = $round;
        return $this;
    }

    /**
     * @param int $currentRound
     * @return bool
     * @throws \Exception
     */
    public function canUseSkill(int $currentRound): bool
    {
        $random = Helper::getRandom(1, 100);

        $canBeApplied = ($random <= $this->getChance() && !$this->getStatus()) || ($this->getStatus()
                && (($currentRound - 1) - $this->getRoundActivated() <= $this->getDuration()));
        if (!$canBeApplied) {
            $this->setStatus(false);
            $this->setRoundActivated(0);
        } else {
            if ($this->getDuration() > 1 && !$this->getStatus()) {
                $this->setStatus(true);
                $this->setRoundActivated($currentRound);
            }
        }

        return $canBeApplied;
    }


}