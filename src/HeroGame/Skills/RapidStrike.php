<?php
/**
 * Created by PhpStorm.
 * User: ainz
 * Date: 10/7/18
 * Time: 1:12 PM
 */

namespace HeroGame\Skills;

use HeroGame\Characters\CharactersAbstract;

/**
 * Class RapidStrike
 *
 * Class for RapidStrike Skill
 *
 * @package HeroGame\Skills
 */
class RapidStrike extends SkillsAbastract implements SkillsInterface
{

    /**
     * @var string
     */
    protected $skillName = 'Rapid Strike';


    /**
     * @var int
     */
    protected $chance = 5;


    /**
     * @var int
     */
    protected $duration = 1;


    /**
     * @var bool
     */
    protected $status = false;

    /**
     * @var int
     */
    protected $roundActivated = 0;

    /**
     * @var int
     */
    protected $type = 1;

    /**
     * @param CharactersAbstract $charact
     * @param int $currentRound
     * @param float $damage
     * @return float
     */
    public function useSkill(CharactersAbstract &$charact, int $currentRound, float $damage): float
    {

        return $damage * 2;

    }

}