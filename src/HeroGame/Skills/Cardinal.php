<?php
/**
 * Created by PhpStorm.
 * User: ainz
 * Date: 10/8/18
 * Time: 8:44 AM
 */

namespace HeroGame\Skills;

use HeroGame\Characters\CharactersAbstract;

/**
 * Additional skill
 * When activated, character recovers 15% of maximum health
 *
 * Class Cardinal
 * @package HeroGame\Skills
 */
class Cardinal extends SkillsAbastract implements SkillsInterface
{
    /**
     * @var string
     */
    protected $skillName = 'Cardinal';

    /**
     * @var int
     */
    protected $chance = 5;

    /**
     * @var int
     */
    protected $duration = 1;


    /**
     * @var bool
     */
    protected $status = false;

    /**
     * @var int
     */
    protected $roundActivated = 0;

    /**
     * @var int
     */
    protected $type = 3;

    /**
     * @param CharactersAbstract $charact
     * @param int $currentRound
     * @param float $damage
     * @return float|int
     */
    public function useSkill(CharactersAbstract &$charact, int $currentRound, float $damage): float
    {

        $maxHealth = $charact->getHealth();
        $healthLeft = $charact->getHealthLeft();
        $recoverValue = (15 / 100) * $maxHealth;
        if ($recoverValue + $healthLeft > $maxHealth) {
            $charact->setHealthLeft($maxHealth);
            $recoverValue = $maxHealth - $healthLeft;
        } else {
            $charact->setHealthLeft($recoverValue + $healthLeft);
        }
        return $recoverValue;

    }
}