<?php
/**
 * Created by PhpStorm.
 * User: ainz
 * Date: 10/7/18
 * Time: 12:43 PM
 */

namespace HeroGame\Game;

/**
 * Class CharactStartingStats
 *
 * In this class we setup characters rules and stats
 * Note: All the stats, skills and names bellow can be removed, modified on game initialization
 *
 * @package HeroGame\Game
 */
class CharactStartingStats
{

    /**
     * Hero name
     */
    const HERO_NAME = 'Orderus';

    /**
     * Hero starting stats
     */
    const HERO_STATS = [
        'health' => [70, 100],
        'strength' => [70, 80],
        'defence' => [45, 55],
        'speed' => [40, 50],
        'luck' => [10, 30]
    ];

    /**
     * Hero starting skills
     */
    const HERO_SKILLS = [
        'RapidStrike' => 10,
        'MagicShield' => 20,
//        'Cardinal' => 12,
    ];


    /**
     * WildBeast name
     * We set as array because we can use the Random Name Generator library for more fun names ^_^
     */
    const BEAST_NAME = [
        'Legless Lizard',
        'Freak Captain'
    ];

    /**
     * WildBeast stats
     */
    const BEAST_STATS = [
        'health' => [60, 90],
        'strength' => [60, 90],
        'defence' => [40, 60],
        'speed' => [40, 60],
        'luck' => [25, 40]
    ];


    /**
     * WilBeast starting skills
     * We just can't let the poor thing defenceless
     * Default no skills are initialized so we can follow the requirements of the game
     * but you can add them here or on game initialization
     */
    const BEAST_SKILLS = [
//        'Cardinal' => 10,
//        'RapidStrike' => 10,
//        'MagicShield' => 20
    ];


}