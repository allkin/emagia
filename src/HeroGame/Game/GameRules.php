<?php
/**
 * Created by PhpStorm.
 * User: ainz
 * Date: 10/7/18
 * Time: 12:58 PM
 */

namespace HeroGame\Game;


/**
 * Class GameRules
 *
 * In this class we define all rules related to game logic, including default environment classes
 *
 * @package HeroGame\Game
 */
class GameRules
{

    /**
     * Number of rounds our fight will take
     */
    const MAX_ROUNDS = 20;

    /**
     * @param int $attackerStr
     * @param int $defenderDef
     * @return int
     */
    public static function calculateDamage(int $attackerStr, int $defenderDef)
    {
        return $attackerStr - $defenderDef;
    }


}