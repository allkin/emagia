<?php
/**
 * Created by PhpStorm.
 * User: ainz
 * Date: 10/7/18
 * Time: 6:26 PM
 */

namespace HeroGame\Game;

/**
 * Helper class for functions shared by multiple classes
 * Class Helper
 * @package HeroGame\Game
 */
class Helper
{

    /**
     * Calculates random in range of two given numbers
     *
     * @param int $min
     * @param int $max
     * @return int
     * @throws \Exception
     */
    static function getRandom(int $min = 0, int $max = 0): int
    {
        if ($min >= $max) {
            throw new \Exception('Invalid values! Minimum should be greater or same as maximum!');
        }
        return mt_rand($min, $max);
    }
}