<?php
/**
 * Created by PhpStorm.
 * User: ainz
 * Date: 10/7/18
 * Time: 11:33 AM
 */

namespace HeroGame\Game;

use HeroGame\Characters\CharactersAbstract;
use HeroGame\Characters\Hero;
use HeroGame\Characters\WildBeast;
use HeroGame\Game\CharactStartingStats as DefaultStats;
use HeroGame\Skills\SkillsAbastract;

/**
 * Class Game
 * @package HeroGame\Game
 */
class HeroGame
{

    const HERO_COLOR = 'blue';

    const BEAST_COLOR = 'purple';

    /**
     * @var Hero
     */
    protected $hero;

    /**
     * @var WildBeast
     */
    protected $wildBeast;

    /**
     * @var $currentRound int
     */
    protected $currentRound = 0;

    /**
     * @var $attacker
     */
    protected $attacker;

    /**
     * @var $defender
     */
    protected $defender;

    /**
     * @var $winner
     */
    protected $winner;

    /**
     * Takes aditional options (see README.md)
     * HeroGame constructor.
     *
     * @param array $options
     * @throws \Exception
     */
    public function __construct($options = [])
    {
        Console::log("Game is starting...", 'bold', 'red');
        $this->initGame($options);
    }


    /**
     * Game initialization
     * Creates the hero and the wild beast and assigns stats
     * and skills to both of them(if it is specified in CharactStartingStats.php)
     *
     * @param array $options
     * @throws \Exception
     */
    private function initGame($options = [])
    {
        $this->createHero();
        $this->createWildBeast();
        $this->logHeroStats();
        $this->logHeroSkills();
        $this->logWildBeastStats();
        $this->logWildBeastSkills();
        $this->processCustomOptions($options);
        $this->decideFirstAttacker();

    }


    /**
     * Game loop method
     * Checks every loop if the characters are alive and the number of rounds didn't reached the maximum allowed
     */
    public function start()
    {
        try {
            $this->currentRound++;
            while ($this->areCharactersAlive() && $this->currentRound <= GameRules::MAX_ROUNDS) {
                Console::log("Round " . $this->currentRound, "dim", "white");
                $this->logCharactersHealth();
                switch (true) {
                    case $this->attacker instanceof Hero:
                        $this->heroAttack();
                        $this->attacker = &$this->wildBeast;
                        break;

                    case $this->attacker instanceof WildBeast:
                        $this->wildBeastAttack();
                        $this->attacker = &$this->hero;
                        break;

                    default:
                        $this->initGame();
                }
                $this->currentRound++;
                Console::logLines();
            }

            $this->decideWinner();

            Console::log("Congratulations " . $this->winner->getName() . "!", "bold", "blue");
        } catch (\Exception $e) {
            Console::log($e->getMessage(), "bold", "red");
        }
    }

    /**
     * Decides first attacker based on SPEED and LUCK if both characters have the same SPEED
     * If both characters have same speed and luck we reinitialize the game with fresh stats
     */
    private function decideFirstAttacker()
    {
        $heroSpeed = $this->hero->getSpeed();
        $wildBeastSpeed = $this->wildBeast->getSpeed();

        if ($heroSpeed > $wildBeastSpeed) $this->attacker = &$this->hero;
        else if ($wildBeastSpeed > $heroSpeed) $this->attacker = &$this->wildBeast;
        else if ($heroSpeed === $wildBeastSpeed) {
            $heroLuck = $this->hero->getLuck();
            $wildBeastLuck = $this->wildBeast->getLuck();
            if ($heroLuck > $wildBeastLuck) $this->attacker = &$this->hero;
            else if ($wildBeastLuck > $heroLuck) $this->attacker = &$this->wildBeast;
        }

        if (empty($this->attacker)) {
            Console::log("What are the ods to get same SPEED & LUCK? Reinitializing the game...", "bold", "red");
            $this->initGame();
        } else {
            Console::log("The first to attack will be " . $this->attacker->getName(), "bold", "red");
            Console::logLines();
        }
    }

    /**
     * Creates the hero object, assigns the default stats and skills
     * Also sets the remaining health equal to initial health
     */
    private function createHero()
    {
        $this->hero = new Hero();

        $this->hero->setName(DefaultStats::HERO_NAME);
        $this->setHeroDefaultStats();
        $this->setHeroDefaultSkills();
        $this->hero->setHealthLeft($this->hero->getHealth());

    }

    /**
     * Sets default stats for hero
     */
    private function setHeroDefaultStats()
    {
        foreach (DefaultStats::HERO_STATS as $stat => $range) {
            $methodName = "set" . ucfirst($stat);
            if (!method_exists($this->hero, $methodName)) {
                Console::log("Invalid stat " . ucwords($stat) . "! Exiting...");
                die;
            }
            try {
                $statValue = Helper::getRandom(reset($range), end($range));
            } catch (\Exception $e) {
                Console::log("Error! " . $e->getMessage(), "bold", "red");
                die;
            }
            $this->hero->{$methodName}($statValue);
        }
    }

    /**
     * Sets default skills for hero
     */
    private function setHeroDefaultSkills()
    {
        foreach (DefaultStats::HERO_SKILLS as $skillName => $chance) {
            if (empty($skillName)) continue;
            $this->hero->addSkill($skillName, $chance);
        }
    }


    /**
     * Creates the wild beast object, assigns the default stats and skills
     * Also sets the remaining health equal to initial health
     */
    private function createWildBeast()
    {
        $this->wildBeast = new WildBeast();
        $this->wildBeast->setName(DefaultStats::BEAST_NAME[array_rand(DefaultStats::BEAST_NAME)]);
        $this->setWildBeastDefaultStats();
        $this->setWildBeastDefaultSkills();
        $this->wildBeast->setHealthLeft($this->wildBeast->getHealth());
    }

    /**
     * Sets default stats for wild beast
     */
    private function setWildBeastDefaultStats()
    {
        foreach (DefaultStats::BEAST_STATS as $stat => $range) {
            $methodName = "set" . ucfirst($stat);
            if (!method_exists($this->wildBeast, $methodName)) {
                Console::log("Invalid stat " . ucwords($stat) . "! Exiting...");
                die;
            }
            try {
                $statValue = Helper::getRandom(reset($range), end($range));
            } catch (\Exception $e) {
                Console::log("Error! " . $e->getMessage(), "bold", "red");
                die;
            }
            $this->wildBeast->{$methodName}($statValue);
        }
    }

    /**
     * Sets default skills for wild beast
     */
    private function setWildBeastDefaultSkills()
    {
        foreach (DefaultStats::BEAST_SKILLS as $skillName => $chance) {
            if (empty($skillName)) continue;
            $this->wildBeast->addSkill($skillName, $chance);
        }
    }

    /**
     * Checking if there are any custom options like Weather and change data accordingly
     *
     * @param array $options
     * @throws \Exception
     */
    private function processCustomOptions(array $options)
    {
        Console::log("Checking custom options...", "bold", "blue");
        if (!empty($options['env'])) {
            foreach ($options['env'] as $env => $class) {
                if ($env === 'weather') {
                    $weatherObject = "HeroGame\\Environment\\" . ucfirst($class) . "Weather";
                    if (!class_exists($weatherObject)) continue;
                    $weatherObject = new $weatherObject();
                    if (!method_exists($weatherObject, 'getChanges')) continue;
                    Console::log("Environment activated: " . ucfirst($class) . "Weather", "bold", "blue");
                    foreach ($weatherObject->getChanges() as $stat => $modifier) {
//                        Console::log("[Env] Modified " . ucwords($stat) . " by " . (is_array($modifier) ? reset($modifier) . ", " . end($modifier) : $modifier) . " for all characters!", "bold", "blue");
                        $setMethodName = "set" . ucfirst($stat);
                        $getMethodName = "get" . ucfirst($stat);
                        if (method_exists(CharactersAbstract::class, $getMethodName)) {
                            $heroModifier = is_array($modifier) ? Helper::getRandom(reset($modifier), end($modifier)) : $modifier;
                            $this->hero->{$setMethodName}($this->hero->$getMethodName() + $heroModifier);
                            Console::log("Changed Hero ".strtoupper($stat)." from " . $this->hero->$getMethodName() .
                                " to " . ($this->hero->$getMethodName() + $heroModifier), "bold", "blue");
                            $wildBeastModifier = is_array($modifier) ? Helper::getRandom(reset($modifier), end($modifier)) : $modifier;
                            $this->wildBeast->{$setMethodName}($this->wildBeast->$getMethodName() + $wildBeastModifier);
                            Console::log("Changed Wild Beast ".strtoupper($stat)." from " . $this->wildBeast->$getMethodName() .
                                " to " . ($this->wildBeast->$getMethodName() + $wildBeastModifier), "bold", "blue");
                        }
                    }
                }
            }
        } else {
            Console::log("No custom options found...", "bold", "blue");
        }
        Console::logLines();
    }


    /**
     * Decides the winner based on both characters remaining health
     */
    private function decideWinner()
    {
        Console::log("AND WE HAVE A WINNER!", "bold", "blue");
        $this->winner = $this->wildBeast->getHealthLeft() > $this->hero->getHealthLeft() ? $this->wildBeast : $this->hero;
    }


    /**
     * Logic for hero's attack
     * We calculate damage based on hero's STR and Wild Beast's DEF
     * We check for ATTACK type skills like Rapid Strike and activate them if the chance occurs
     * We check for DEFENCE type skills for the defender
     * We check if the defender is lucky enough and don't get hit
     * We commence the attack based on calculated damage + skills if active
     * We check for RECOVER type skills for the attacker
     *
     * @return bool
     * @throws \Exception
     */
    private function heroAttack(): bool
    {
        $damage = GameRules::calculateDamage($this->hero->getStrength(), $this->wildBeast->getDefence());
        $heroSkills = $this->hero->getSkills();

        if (!empty($heroSkills[SkillsAbastract::RECOVER])) {
            foreach ($heroSkills[SkillsAbastract::RECOVER] as $skill) {
                if ($skill->canUseSkill($this->currentRound)) {
                    $recovered = $skill->useSkill($this->hero, $this->currentRound, $damage);
                    $this->logRecoverSkill($this->hero->getName(), $skill->getSkillName(), $recovered, static::HERO_COLOR);
                }
            }
        }

        $this->logAttack($this->hero->getName(), $damage, static::HERO_COLOR);


        if (!empty($heroSkills[SkillsAbastract::ATTACK])) {
            foreach ($heroSkills[SkillsAbastract::ATTACK] as $skill) {
                if ($skill->canUseSkill($this->currentRound)) {
                    $damage = $skill->useSkill($this->wildBeast, $this->currentRound, $damage);
                    $this->logAttackSkill($this->hero->getName(), $skill->getSkillName(), $damage, static::HERO_COLOR);
                }
            }
        }

        $wildBeastSkills = $this->wildBeast->getSkills();

        if (!empty($wildBeastSkills[SkillsAbastract::DEFENCE])) {
            foreach ($wildBeastSkills[SkillsAbastract::DEFENCE] as $skill) {
                if ($skill->canUseSkill($this->currentRound)) {
                    $damage = $skill->useSkill($this->wildBeast, $this->currentRound, $damage);
                    $this->logDefenceSkill($this->wildBeast->getName(), $skill->getSkillName(), $damage, static::BEAST_COLOR);

                }
            }
        }


        if ($this->wildBeast->isLuckyThisRound()) {
            $this->logMiss($this->hero->getName(), static::HERO_COLOR);
            return false;
        }

        $healthLeft = $this->wildBeast->getHealthLeft() - $damage;
        $this->wildBeast->setHealthLeft($healthLeft);
        $this->logHit($this->wildBeast->getName(), $damage, $healthLeft, $this->wildBeast->getHealth(), static::BEAST_COLOR);


        return true;

    }

    /**
     * Logic for Wild beast's attack
     * We calculate damage based on Wild beast's STR and Hero's DEF
     * We check for ATTACK type skills like Rapid Strike and activate them if the chance occurs
     * We check for DEFENCE type skills for the defender
     * We check if the defender is lucky enough and don't get hit
     * We commence the attack based on calculated damage + skills if active
     * We check for RECOVER type skills for the attacker
     *
     * @return bool
     * @throws \Exception
     */
    private function wildBeastAttack(): bool
    {
        $damage = GameRules::calculateDamage($this->wildBeast->getStrength(), $this->hero->getDefence());

        $wildBeastSkills = $this->wildBeast->getSkills();
        if (!empty($wildBeastSkills[SkillsAbastract::RECOVER])) {
            foreach ($wildBeastSkills[SkillsAbastract::RECOVER] as $skill) {
                if ($skill->canUseSkill($this->currentRound)) {
                    $recovered = $skill->useSkill($this->wildBeast, $this->currentRound, $damage);
                    $this->logRecoverSkill($this->wildBeast->getName(), $skill->getSkillName(), $recovered, static::BEAST_COLOR);
                }

            }
        }

        $this->logAttack($this->wildBeast->getName(), $damage, static::BEAST_COLOR);

        if (!empty($wildBeastSkills[SkillsAbastract::ATTACK])) {
            foreach ($wildBeastSkills[SkillsAbastract::ATTACK] as $skill) {
                if ($skill->canUseSkill($this->currentRound)) {
                    $damage = $skill->useSkill($this->hero, $this->currentRound, $damage);
                    $this->logAttackSkill($this->wildBeast->getName(), $skill->getSkillName(), $damage, static::BEAST_COLOR);
                }
            }
        }

        $heroSkills = $this->hero->getSkills();
        if (!empty($heroSkills[SkillsAbastract::DEFENCE])) {
            foreach ($heroSkills[SkillsAbastract::DEFENCE] as $skill) {
                if ($skill->canUseSkill($this->currentRound)) {
                    $damage = $skill->useSkill($this->hero, $this->currentRound, $damage);
                    $this->logDefenceSkill($this->hero->getName(), $skill->getSkillName(), $damage, static::HERO_COLOR);

                }
            }
        }

        if ($this->hero->isLuckyThisRound()) {
            $this->logMiss($this->wildBeast->getName(), static::BEAST_COLOR);
            return false;
        }

        $healthLeft = $this->hero->getHealthLeft() - $damage;
        $this->hero->setHealthLeft($healthLeft);

        $this->logHit($this->hero->getName(), $damage, $healthLeft, $this->hero->getHealth(), static::HERO_COLOR);

        return true;

    }

    /**
     * Checking if the characters are alive
     *
     * @return bool
     */
    private function areCharactersAlive(): bool
    {
        return $this->hero->isAlive() && $this->wildBeast->isAlive();
    }


    #################
    ## LOG METHODS ##
    #################

    /**
     * Logs hero's stats
     */
    private function logHeroStats()
    {
        Console::logLines();
        Console::log("Hero " . $this->hero->getName() . " created!", "green");
        foreach (DefaultStats::HERO_STATS as $stat => $range) {
            $methodName = "get" . ucfirst($stat);
            Console::log(ucwords($stat) . ': ' . $this->hero->{$methodName}(), "green");
        }
    }


    /**
     * Logs hero's skills
     *
     * @return bool
     */
    private function logHeroSkills(): bool
    {
        if (empty($this->hero->getSkills())) {
            Console::log("No skills for hero :(", "red");
            return false;
        } else {
            Console::log("Skills: ", "cyan");
        }
        foreach ($this->hero->getSkills() as $type => $skills) {
            foreach ($skills as $skill) {
                $name = $skill->getSkillName();
                $chance = $skill->getChance();
                $duration = $skill->getDuration();

                Console::log("NAME: $name | DURATION: $duration" . ($duration > 1 ? " rounds" : " round")
                    . " | CHANCE: $chance%", 'cyan');
            }
        }
        Console::logLines();
        return true;
    }


    /**
     * Logs wild beast's stats
     */
    private function logWildBeastStats()
    {
        Console::log("Wild Beast " . $this->wildBeast->getName() . " created!", "green");
        foreach (DefaultStats::BEAST_STATS as $stat => $range) {
            $methodName = "get" . ucfirst($stat);
            Console::log(ucwords($stat) . ": " . $this->wildBeast->{$methodName}(), "green");
        }
    }

    /**
     * Logs wild beast's skills
     *
     * @return bool
     */
    private function logWildBeastSkills(): bool
    {
        if (empty($this->wildBeast->getSkills())) {
            Console::log("No skills for wild beast :(", "red");
            Console::logLines();
            return false;
        } else {
            Console::log("Skills: ", "cyan");
        }
        foreach ($this->wildBeast->getSkills() as $type => $skills) {
            foreach ($skills as $skill) {
                $name = $skill->getSkillName();
                $chance = $skill->getChance();
                $duration = $skill->getDuration();

                Console::log("NAME: $name | DURATION: $duration" . ($duration > 1 ? " rounds" : " round")
                    . " | CHANCE: $chance%", "cyan");
            }
        }
        Console::logLines();
        return true;
    }


    /**
     * Logs ATTACK type skills
     *
     * @param string $characterName
     * @param string $skillName
     * @param float $damage
     * @param string $characterColor
     */
    private function logAttackSkill(string $characterName, string $skillName, float $damage, string $characterColor)
    {
        Console::log($characterName, $characterColor, false);
        Console::log(" activates ", "normal", false);
        Console::log(ucwords($skillName), "cyan", false);
        Console::log("! Damage increases to ", "normal", false);
        Console::log($damage, "red");
    }

    /**
     * Logs DEFENCE type skills
     *
     * @param string $characterName
     * @param string $skillName
     * @param float $damage
     * @param string $characterColor
     */
    private function logDefenceSkill(string $characterName, string $skillName, float $damage, string $characterColor)
    {
        Console::log($characterName, $characterColor, false);
        Console::log(" activates ", "normal", false);
        Console::log(ucwords($skillName), "cyan", false);
        Console::log("! Damaged is reduced to ", "normal", false);
        Console::log($damage, "red");
    }

    /**
     * Logs RECOVER type skills
     *
     * @param string $characterName
     * @param string $skillName
     * @param float $recoveredHealth
     * @param string $characterColor
     */
    private function logRecoverSkill(string $characterName, string $skillName, float $recoveredHealth, string $characterColor)
    {
        Console::log("SURPRISE! ", "green", false);
        Console::log($characterName, $characterColor, false);
        Console::log(" activates the recovering skill ", "normal", false);
        Console::log(ucwords($skillName), "cyan", false);
        Console::log("! Health recovered: ", "normal", false);
        Console::log($recoveredHealth, "green");
    }

    /**
     * Logs character hit
     *
     * @param string $characterName
     * @param string $damage
     * @param float $currentHealth
     * @param float $originalHealth
     * @param string $characterColor
     */
    private function logHit(string $characterName, string $damage, float $currentHealth, float $originalHealth, string $characterColor)
    {
        Console::log("STRIKE! ", "green", false);
        Console::log($characterName, $characterColor, false);
        Console::log(" receives ", "normal", false);
        Console::log($damage, "red", false);
        Console::log(" damage! ", "normal", false);
        Console::log("Health left: ", "normal", false);
        Console::log(($currentHealth < 0 ? 0 : $currentHealth) . "/" . $originalHealth, "green");

    }

    /**
     * Logs character initial attack
     *
     * @param string $characterName
     * @param float $damage
     * @param string $characterColor
     */
    private function logAttack(string $characterName, float $damage, string $characterColor)
    {
        Console::log($characterName, $characterColor, false);
        Console::log(" attacks with a damage of ", 'normal', false);
        Console::log($damage, "red");
    }

    /**
     * Logs character miss
     *
     * @param string $characterName
     * @param string $characterColor
     */
    private function logMiss(string $characterName, string $characterColor)
    {
        Console::log("MISS! ", 'red', false);
        Console::log($characterName, $characterColor, false);
        Console::log(" didn't managed to land a hit! ", 'normal');
    }

    private function logCharactersHealth()
    {
        Console::log($this->hero->getName() . " HP: " . $this->hero->getHealthLeft() .
            " | " . $this->wildBeast->getName() . " HP: " . $this->wildBeast->getHealthLeft(), 'green');
    }

}