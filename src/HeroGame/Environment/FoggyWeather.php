<?php
/**
 * Created by PhpStorm.
 * User: ainz
 * Date: 10/7/18
 * Time: 2:58 PM
 */

namespace HeroGame\Environment;

/**
 * Class FoggyWeather
 * @package HeroGame\Environment
 */
class FoggyWeather extends WeatherAbstract
{

    /**
     * Default changes on foggy weather
     * @var int
     */
    protected $changes = [
        'speed' => [-10, -5],
        'strength' => [-10, -3]
    ];

}