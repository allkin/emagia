<?php
/**
 * Created by PhpStorm.
 * User: ainz
 * Date: 10/7/18
 * Time: 12:25 PM
 */

namespace HeroGame\Environment;

/**
 * Class World
 * Another deviation from requirements.
 * With environment implementation we can create a more dynamic battle
 * Based on the weather, a character's speed can increase (sunny day, it increases his adrenalin)
 * or decrease (foggy weather, no visibility)
 * Note: This implementation is optional. It can be disabled on game initialization.
 * @package HeroGame\Environment
 */
abstract class WeatherAbstract
{

    /**
     * @var $changes array
     */
    protected $changes;

    /**
     * @return array
     */
    public function getChanges(): array
    {
        return $this->changes;
    }

}