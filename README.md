**Hero game that takes place in a fantasy land named Emagia.**

## Requirements

* PHP 7.x
* Composer 

## Optional requirements

* PHPUnit 7.x

## Installation and run

*In folder root you need to type the following commands:*  
```composer install```  
```php index.php```


**This game was tested and coded to run on termnial. It should run on browser too but it would look ugly because of the PHP Colored Cli class.**

**Story:**  
Our hero Orderus battles a Wild Beast with a random name (lol).  
Each character is generated with random stats and skills.  
Game is turn based, the first attacker is decided based on the SPEED or LUCK (if SPEED is the same for both) of characters.  

*Note: This version of the game contains some extra options like environment and 3 types of skills for both characters. **By default, the game follows the documentation and all additional options are disabled.***  

---

## Additional features:  

**Custom options:**  
On game object instantiation you can pass as first parameter an array with custom options.  
For now, only the **environment** option is checked but it can be extended to skills, stats and much more.  
**Code:**  
```
$options = [
	"env" => [
		"weather" => "foggy"
	]
];
```
*The weather environment modifies stats for characters based on weather conditions. For example: On foggy weather, the speed and strength of both characters is reduced by a random value.*  

**Skill type:**  
**On this version of the game the skills are (optionally) shared by both characters.**  
By default, only Orderus can use skills but you can activate them for Wild Beast too by uncomment the skills lines in ``` Game\CharactStartingStats.php```  
There are 3 types of skills:  
a. **ATTACK**   
b. **DEFENCE**  
c. **RECOVER**  
By adding these types of skills we can easily implement new skills that can manipulate not only damage but health or other stats.  
For example I added a 3rd skill named **Cardinal** which recovers **15%** of user's maximum health.  
**Rapid Strike** is a **attack** skill that activates (by chance) after the initial damage is calculated.  
**Magic Shield** is a **defence** skill that activates (by chance) for defender after the final damage(skills included) is calculated.  
**Cardinal** is a **recover** skill that activates (by chance) at the start of the round.    
*All skills have a default **chance** value that is replaced at the start of the game.*

**Skill duration:**  
All types of skills, when activated, can last for **N** rounds. **By default, all skills last 1 round but can be changed easily in each skill's class**  

Character stats and skills can be changed in ``` Game\CharactStartingStats.php```.  
Game rounds and damage formula can be changed in ``` Game\GameRules.php```.  
Skills settings can be changed in ```Skills\SkillName.php```.  

## 3rd party libraries/classes:
* [PHP Colored CLI](https://github.com/donatj) by Jesse Donat