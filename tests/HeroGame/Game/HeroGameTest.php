<?php
/**
 * Created by PhpStorm.
 * User: ainz
 * Date: 10/7/18
 * Time: 10:10 PM
 */

namespace Tests\HeroGame\Game;

use PHPUnit\Framework\TestCase;

/**
 * Class HeroGameTest
 * @package Tests\HeroGame\Game
 */
class HeroGameTest extends TestCase
{
    protected $hero;

    protected $wildBeast;


    /**
     * Test for method that check that both player are alive
     */
    public function testPlayersAliveTrue()
    {
        $hero = $this->hero;
        self::assertTrue($hero->getHealthLeft() > 0, 'Hero is still alive');
        $wildBeast = $this->wildBeast;
        self::assertTrue($wildBeast->getHealthLeft() > 0, 'Wild Beast is still alive');
    }

    public function testSkillsExistTrue()
    {
        $hero = $this->hero;
        self::assertTrue(count($hero->getSkills()) > 0, 'Hero has ' . count($hero->getSkills()));
        $wildBeast = $this->wildBeast;
        self::assertTrue(count($wildBeast->getSkills()) > 0, 'Hero has ' . count($wildBeast->getSkills()));
    }


    /**
     * Set up test Characters
     */
    protected function setUp()
    {
        $hero = $this
            ->getMockBuilder('HeroGame\Characters\Hero')
            ->getMockForAbstractClass();
        $hero
            ->setHealth(41)
            ->setHealthLeft(41)
            ->setStrength(23)
            ->setDefence(34)
            ->setSpeed(57)
            ->setLuck(12)
            ->addSkill('MagicShield', 10)
            ->addSkill('RapidStrike', 25);
        $this->hero = $hero;
        $wildBeast = $this
            ->getMockBuilder('HeroGame\Characters\WildBeast')
            ->getMockForAbstractClass();
        $wildBeast
            ->setHealth(44)
            ->setHealthLeft(44)
            ->setStrength(21)
            ->setDefence(33)
            ->setSpeed(50)
            ->setLuck(15)
            ->addSkill('RapidStrike', 5)
            ->addSkill('MagicShield', 10);
        $this->wildBeast = $wildBeast;
    }
}