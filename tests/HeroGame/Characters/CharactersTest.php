<?php
/**
 * Created by PhpStorm.
 * User: ainz
 * Date: 10/7/18
 * Time: 10:03 PM
 */

namespace Tests\HeroGame\Characters;

use PHPUnit\Framework\TestCase;


/**
 * Class CharactersTest
 * @package Tests\HeroGame\Characters
 */
class CharactersTest extends TestCase
{

    protected $character;

    public function testGetSetHealth()
    {
        $character = $this->character;
        self::assertEquals(90, $character->getHealth());
        $character->setHealth(44);
        self::assertEquals(44, $character->getHealth());
    }

    /**
     * Test Strength get/set methods
     */
    public function testGetSetStrength()
    {
        $character = $this->character;
        self::assertEquals(40, $character->getStrength());
        $character->setStrength(44);
        self::assertEquals(44, $character->getStrength());
    }

    /**
     * Test Defence get/set methods
     */
    public function testGetSetDefence()
    {
        $character = $this->character;
        self::assertEquals(45, $character->getDefence());
        $character->setDefence(55);
        self::assertEquals(55, $character->getDefence());
    }

    /**
     * Test Speed get/set methods
     */
    public function testGetSetSpeed()
    {
        $character = $this->character;
        self::assertEquals(35, $character->getSpeed());
        $character->setSpeed(34);
        self::assertEquals(34, $character->getSpeed());
    }

    /**
     * Test Luck get/set methods
     */
    public function testGetSetLuck()
    {
        $character = $this->character;
        self::assertEquals(22, $character->getLuck());
        $character->setLuck(21);
        self::assertEquals(21, $character->getLuck());
    }

    protected function setUp()
    {
        $character = $this->getMockBuilder('HeroGame\Characters\CharactersAbstract')->getMockForAbstractClass();
        $character->setHealth(90);
        $character->setHealthLeft(90);
        $character->setStrength(40);
        $character->setDefence(45);
        $character->setSpeed(35);
        $character->setLuck(22);

        $this->character = $character;
    }
}