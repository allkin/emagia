<?php
/**
 * Created by PhpStorm.
 * User: ainz
 * Date: 10/7/18
 * Time: 10:47 PM
 */


namespace Tests\HeroGame\Skills;

use PHPUnit\Framework\TestCase;


class SkillsTest extends TestCase
{

    /**
     * @var $rapidStrike object
     */
    protected $rapidStrike;

    /**
     * @var $magicShield object
     */
    protected $magicShield;


    public function testGetDuration()
    {
        self::assertEquals(1, $this->rapidStrike->getDuration());
        self::assertEquals(1, $this->magicShield->getDuration());
    }

    public function testGetSetChance()
    {
        self::assertEquals(5, $this->rapidStrike->getChance());
        $this->rapidStrike->setChance(10);
        self::assertEquals(10, $this->rapidStrike->getChance());
        $this->magicShield->setChance(12);
        self::assertEquals(12, $this->magicShield->getChance());
    }

    public function testGetSetStatus()
    {
        self::assertFalse($this->rapidStrike->getStatus());
        $this->rapidStrike->setStatus(true);
        self::assertTrue($this->rapidStrike->getStatus());
    }

    public function testGetSetRoundActivated()
    {
        self::assertEquals(0, $this->magicShield->getRoundActivated());
        $this->magicShield->setRoundActivated(5);
        self::assertTrue($this->magicShield->getRoundActivated() === 5);
    }

    public function testCanUseSkill()
    {
        $this->rapidStrike->setChance(100);
        self::assertTrue($this->rapidStrike->canUseSkill(5));

        $this->rapidStrike->setChance(0);
        self::assertFalse($this->rapidStrike->canUseSkill(2));

        $this->rapidStrike->setChance(100);
        $this->rapidStrike->setStatus(true);
        $this->rapidStrike->setRoundActivated(5);
        self::assertTrue($this->rapidStrike->canUseSkill(6));

    }

    public function testUseSkillRapidStrike()
    {
        $hero = $this->getMockBuilder('HeroGame\Characters\Hero')->getMockForAbstractClass();

        $hero->setHealth(41)
            ->setHealthLeft(41)
            ->setStrength(23)
            ->setDefence(34)
            ->setSpeed(57)
            ->setLuck(12)
            ->addSkill('MagicShield', 10)
            ->addSkill('RapidStrike', 25);

        $damage = 5;
        $this->rapidStrike->setChance(100);
        $damage = $this->rapidStrike->useSkill($hero, 5, $damage);
        self::assertEquals(10, $damage);
    }

    public function testUseSkillMagicShield()
    {
        $hero = $this->getMockBuilder('HeroGame\Characters\Hero')->getMockForAbstractClass();

        $hero->setHealth(41)
            ->setHealthLeft(41)
            ->setStrength(23)
            ->setDefence(34)
            ->setSpeed(57)
            ->setLuck(12)
            ->addSkill('MagicShield', 10)
            ->addSkill('RapidStrike', 25);

        $damage = 10;
        $this->magicShield->setChance(100);
        $damage = $this->magicShield->useSkill($hero, 15, $damage);
        self::assertEquals(5, $damage);
    }

    protected function setUp()
    {
        $this->rapidStrike = $this->getMockBuilder('HeroGame\Skills\RapidStrike')
            ->getMockForAbstractClass();
        $this->magicShield = $this->getMockBuilder('HeroGame\Skills\MagicShield')
            ->getMockForAbstractClass();

    }
}